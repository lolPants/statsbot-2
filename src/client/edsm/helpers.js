const { BigNumber } = require('bignumber.js')
const snekfetch = require('snekfetch')
const humanise = require('humanize-plus')

/**
 * @param {Coords} coords Coordinates
 * @param {Coords} [offset] Offset Coordinates
 * @returns {number}
 */
const distanceTo = (coords, offset = { x: 0, y: 0, z: 0 }) => {
  // Calculate Distance to Sol
  let xSq = new BigNumber(coords.x - offset.x).pow(2)
  let ySq = new BigNumber(coords.y - offset.y).pow(2)
  let zSq = new BigNumber(coords.z - offset.z).pow(2)
  return xSq.plus(ySq)
    .plus(zSq)
    .sqrt()
    .toNumber()
}

/**
 * @typedef {Object} EDSM
 * @property {string} name
 * @property {number} id
 * @property {Coords} coords
 * @property {boolean} requirePermit
 * @property {Information} information
 * @property {PrimaryStar} primaryStar
 * @property {Body[]} bodies
 * @property {Station[]} stations
 * @property {Faction[]} factions
 * @property {ControllingFaction} controllingFaction
 */

/**
 * @typedef {Object} Coords
 * @property {number} x
 * @property {number} y
 * @property {number} z
 */

/**
 * @typedef {Object} Information
 * @property {string} allegiance
 * @property {string} government
 * @property {string} faction
 * @property {string} factionState
 * @property {number} population
 * @property {string} reserve
 * @property {string} security
 * @property {string} economy
 */

/**
 * @typedef {Object} PrimaryStar
 * @property {string} type
 * @property {string} name
 * @property {boolean} isScoopable
 */

/**
 * @typedef {Object} ControllingFaction
 * @property {number} id
 * @property {string} name
 */

/**
 * @typedef {Object} Body
 * @property {number} id
 * @property {string} name
 * @property {string} type
 * @property {string} subType
 * @property {number} offset
 * @property {number} distanceToArrival
 * @property {number} surfaceTemperature
 * @property {number} orbitalPeriod
 * @property {number} semiMajorAxis
 * @property {number} orbitalEccentricity
 * @property {number} orbitalInclination
 * @property {number} argOfPeriapsis
 * @property {number} rotationalPeriod
 * @property {boolean} rotationalPeriodTidallyLocked
 */

/**
 * @typedef {Object} Station
 * @property {number} id
 * @property {string} name
 * @property {string} type
 * @property {number} distanceToArrival
 * @property {string} allegiance
 * @property {string} government
 * @property {string} economy
 * @property {boolean} haveMarket
 * @property {boolean} haveShipyard
 * @property {ControllingFaction} controllingFaction
 */

/**
 * @typedef {Object} Faction
 * @property {number} id
 * @property {string} name
 * @property {number} influence
 * @property {string} state
 * @property {boolean} isPlayer
 */

/**
 * @param {string} systemName EDSM System Name
 * @returns {Promise.<EDSM>}
 */
const getSystemData = async systemName => {
  // Test Object
  let test = await snekfetch.get('https://www.edsm.net/api-v1/system')
    .query({ systemName })

  // Convert response to Object
  let obj = JSON.parse(test.text)
  // Check if it's an empty array (system not found)
  if (Array.isArray(obj) && obj.length === 0) throw new Error('Invalid System Name')

  // Fetch main data
  let mainRes = await snekfetch.get('https://www.edsm.net/api-v1/system')
    .query({ systemName })
    .query({
      showId: 1,
      showCoordinates: 1,
      showPermit: 1,
      showInformation: 1,
      showPrimaryStar: 1,
      includeHidden: 1,
    })
  // Store this as an object
  let data = JSON.parse(mainRes.text)

  // Setup extra requests
  const endpoints = [
    'https://www.edsm.net/api-system-v1/bodies',
    'https://www.edsm.net/api-system-v1/stations',
    'https://www.edsm.net/api-system-v1/factions',
  ].map(x =>
    // Return a snekfetch query
    snekfetch.get(x)
      .query({ systemName })
  )

  // Await all requests to finish
  let resp = await Promise.all(endpoints)
  // Map to parse JSON responses
  resp = resp.map(x => JSON.parse(x.text))

  // Set relevant data
  data.bodies = resp[0].bodies
  data.stations = resp[1].stations
  data.factions = resp[2].factions
  data.controllingFaction = resp[2].controllingFaction

  // Return
  return data
}

/**
 * Formats system coordinates
 * @param {Coords} coords System Coordinates
 * @returns {string}
 */
const formatCoords = coords => {
  let str = `**X:** \`${humanise.formatNumber(coords.x)}\`
**Y:** \`${humanise.formatNumber(coords.y)}\`
**Z:** \`${humanise.formatNumber(coords.z)}\``
  return str
}

module.exports = {
  distanceTo,
  getSystemData,
  formatCoords,
}

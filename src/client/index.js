const fs = require('fs-extra')
const path = require('path')
const Backend = require('statsbot-backend')
const exitHook = require('async-exit-hook')
const Registry = require('./registry')

// Environment Variables
const { TOKEN, PREFIX, OWNER, AVATAR, STATUS, BANNED_SERVERS } = process.env

const bot = new Backend.Client()
  // Meta Data
  .setName('StatsBot v2')
  .setAuthor('lolPants#0001')
  .setDescription('Overwatch and Elite Dangerous Stats for Discord.')
  // Command Registries
  .addRegistry(Backend.StandardRegistry)
  .addRegistry(Registry)
  // Bot Runtime
  .setPrefix(PREFIX)
  .addOwner(OWNER)
  // Bot Statuses
  .addStatus(STATUS !== undefined ? JSON.parse(STATUS) : [])
  // Banned Servers
  .setBannedGuilds(BANNED_SERVERS !== undefined ? BANNED_SERVERS.split(',') : [])

const AVATAR_PATH = path.join('src/shared/avatar', AVATAR)
fs.readFile(AVATAR_PATH)
  .then(res => { bot.setAvatar(res) })
  .then(() => { bot.login(TOKEN) })
  .catch(console.error)

/**
 * Logout Function
 * Handles CTRL+C and PM2
 */
exitHook(async exit => {
  // Check if logged in
  if (bot.readyAt !== null) {
    try {
      await bot.destroy()
      exit()
    } catch (err) { exit() }
  } else { exit() }
})

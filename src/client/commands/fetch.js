// Package Dependencies
const OWStats = require('overwatch-stats')
const Discord = require('discord.js')
const Backend = require('statsbot-backend')

// JSDoc Typing
const Command = Backend.Command // eslint-disable-line
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

// Colour Data
const LEVEL_COLOURS = {
  BRONZE: '#ad5d4a',
  SILVER: '#ccd4d6',
  GOLD: '#e4b849',
  PLATINUM: '#ccd3d4',
}

// Globals
const REGIONS = ['eu', 'us', 'kr']

/**
 * @param {CommandProperties} data Command Properties
 */
const MainPC = async data => {
  if (data.arguments.length === 0) {
    // No Paramters
    data.message.channel.send(
      new Backend.MessageBuilder({
        message: `You didn't specify any arguments. Use \`${data.client.prefix} help\` to list required arguments.`,
      })
        .setName('Fetch')
        .setError()
        .formattedMessage
    )
  } else if (data.arguments.length === 1) {
    const battletag = data.arguments[0]
    if (!OWStats._checkBattleTag(battletag)) {
      data.message.channel.send(
        new Backend.MessageBuilder({
          message: ':x: Invalid BattleTag!',
        })
          .setName('Fetch')
          .setError()
          .formattedMessage
      )
    } else {
      // Start Typing
      data.message.channel.startTyping()

      try {
        let stats = await OWStats.load(battletag)
        stats = await new OWStats(stats).getData()
        sendStats(data, stats)
      } catch (err) {
        let errMsg = new Backend.MessageBuilder()
          .setError()
          .setName('Fetch')
        switch (err.status) {
          case 404:
            errMsg.setMessage(':x: Error... Player not found!')
            break
          case 429:
            errMsg.setMessage(':x: Error... Bot is being ratelimited. Try Again Later!')
            break
          default:
            errMsg.setMessage(`:x: Error... Try again!\n${err.message}`)
            break
        }
        await data.message.channel.stopTyping()
        data.message.channel.send(errMsg.formattedMessage)
        console.error(err)
      }
    }
  } else {
    // Define Constants
    const battletag = data.arguments[0]
    const region = data.arguments[1].toLowerCase()

    // Check Region Argument is Valid
    if (REGIONS.indexOf(region.toLowerCase()) === -1) {
      // Invalid Region
      data.message.channel.send(
        data.message.channel.send(
          new Backend.MessageBuilder({
            message: 'Invalid Region. Available regions are `eu`, `us` or `kr`',
          })
            .setName('Fetch')
            .setError()
            .formattedMessage
        )
      )
    } else if (!OWStats._checkBattleTag(battletag)) {
      data.message.channel.send(
        data.message.channel.send(
          new Backend.MessageBuilder({
            message: 'Invalid BattleTag!',
          })
            .setName('Fetch')
            .setError()
            .formattedMessage
        )
      )
    } else {
      // Start Typing
      data.message.channel.startTyping()

      try {
        let stats = await OWStats.load(battletag)
        stats = await new OWStats(stats).getData(region)
        sendStats(data, stats)
      } catch (err) {
        let errMsg = new Backend.MessageBuilder()
          .setError()
          .setName('Fetch')
        switch (err.status) {
          case 404:
            errMsg.setMessage(':x: Error... Player not found!')
            break
          case 429:
            errMsg.setMessage(':x: Error... Bot is being ratelimited. Try Again Later!')
            break
          default:
            errMsg.setMessage(`:x: Error... Try again!\n${err.message}`)
            break
        }
        await data.message.channel.stopTyping()
        data.message.channel.send(errMsg.formattedMessage)
        console.error(err)
      }
    }
  }
}

/**
 * @param {CommandProperties} data Command Properties
 */
const MainXBL = async data => {
  if (data.arguments.length === 0) {
    // No Paramters
    data.message.channel.send(
      new Backend.MessageBuilder({
        message: `You didn't specify any arguments. Use \`${data.client.prefix} help\` to list required arguments.`,
      })
        .setName('Fetch')
        .setError()
        .formattedMessage
    )
  } else {
    const battletag = data.arguments.join(' ')

    data.message.channel.startTyping()
    try {
      let stats = await OWStats.load(battletag, 'xbl')
      stats = await new OWStats(stats).getData()
      sendStats(data, stats)
    } catch (err) {
      await data.message.channel.stopTyping()
      data.message.channel.send(
        new Backend.MessageBuilder({
          message: 'Invalid BattleTag!',
        })
          .setName('Fetch')
          .setError()
          .formattedMessage
      )
    }
  }
}

/**
 * @param {CommandProperties} data Command Properties
 */
const MainPSN = async data => {
  if (data.arguments.length === 0) {
    // No Paramters
    data.message.channel.send(
      new Backend.MessageBuilder({
        message: `You didn't specify any arguments. Use \`${data.client.prefix} help\` to list required arguments.`,
      })
        .setName('Fetch')
        .setError()
        .formattedMessage
    )
  } else {
    const battletag = data.arguments.join(' ')

    data.message.channel.startTyping()
    try {
      let stats = await OWStats.load(battletag, 'psn')
      stats = await new OWStats(stats).getData()
      sendStats(data, stats)
    } catch (err) {
      await data.message.channel.stopTyping()
      data.message.channel.send(
        new Backend.MessageBuilder({
          message: 'Invalid BattleTag!',
        })
          .setName('Fetch')
          .setError()
          .formattedMessage
      )
    }
  }
}

/**
 * Send Stats Object
 * @param {CommandProperties} data Command Properties
 * @param {Object} body Stats Data Body
 */
const sendStats = async (data, body) => {
  // Determine Colour
  let colour = ''
  if (body.stats.overall_stats.level_full < 601) {
    colour = LEVEL_COLOURS.BRONZE
  } else if (body.stats.overall_stats.level_full < 1201) {
    colour = LEVEL_COLOURS.SILVER
  } else if (body.stats.overall_stats.level_full < 1801) {
    colour = LEVEL_COLOURS.GOLD
  } else if (body.stats.overall_stats.level_full < 2401) {
    colour = LEVEL_COLOURS.PLATINUM
  } else {
    colour = null
  }

  let regionDisplay = ''
  if (body.stats.user_data.platform === 'pc') regionDisplay = body.stats.user_data.region.toUpperCase()
  else regionDisplay = body.stats.user_data.platform.toUpperCase()

  let embed
  if (body.stats.competitive !== null) {
    embed = new Discord.MessageEmbed()
      .setTitle(body.stats.user_data.battletag)
      .setAuthor(body.stats.user_data.username, body.stats.overall_stats.avatar)
      .setDescription(`Stats for ${body.stats.user_data.battletag} (${regionDisplay})`)
      .setImage(body.stats.overall_stats.avatar)
      .setThumbnail(body.stats.overall_stats.tier_image)
      .setTimestamp()
      .setColor(colour)
      .setURL(encodeURI(body.stats.user_data.user_profile))
      .addField(`Player Level`, body.stats.overall_stats.level_full, true)
      .addField(`Competitive Rank`, `${body.stats.overall_stats.comprank} SR`, true)
      .addField(`Quickplay Wins`, body.stats.quickplay.overall_stats.wins)
      .addField(`Competitive Wins`, body.stats.competitive.overall_stats.wins, true)
      .addField(`Competitive Losses / Draws`, body.stats.competitive.overall_stats.losses, true)
      .addField(`Competitive Total Games`, body.stats.competitive.overall_stats.games)
      .addField(`Quickplay Playtime`, `${body.stats.quickplay.game_stats.time_played} Hours`, true)
      .addField(`Competitive Playtime`, `${body.stats.competitive.game_stats.time_played} Hours`, true)
  } else {
    embed = new Discord.MessageEmbed()
      .setTitle(body.stats.user_data.battletag)
      .setAuthor(body.stats.user_data.username, body.stats.overall_stats.avatar)
      .setDescription(`Stats for ${body.stats.user_data.battletag} (${regionDisplay})`)
      .setImage(body.stats.overall_stats.avatar)
      .setTimestamp()
      .setColor(colour)
      .setURL(encodeURI(body.stats.user_data.user_profile))
      .addField(`Player Level`, body.stats.overall_stats.level_full, true)
      .addField(`Quickplay Wins`, body.stats.quickplay.overall_stats.wins)
      .addField(`Quickplay Playtime`, `${body.stats.quickplay.game_stats.time_played} Hours`, true)
  }

  await data.message.channel.stopTyping()
  data.message.channel.send('', { embed: embed })
}

const FetchPC = new Backend.Command()
  .setName('pc')
  .setDescription('Gets Overwatch Stats for a PC BattleTag')
  .addArgument([
    'battletag',
    '?region',
  ])
  .setMain(MainPC)

const FetchXBL = new Backend.Command()
  .setName('xbl')
  .setDescription('Gets Overwatch Stats for an XBox Gamertag')
  .addArgument('gamertag')
  .setMain(MainXBL)

const FetchPSN = new Backend.Command()
  .setName('psn')
  .setDescription('Gets Overwatch Stats for a Playstation Network ID')
  .addArgument('PSN ID')
  .setMain(MainPSN)

/**
 * Help Command
 */
module.exports = {
  FetchPC,
  FetchXBL,
  FetchPSN,
}

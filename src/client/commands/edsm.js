const Backend = require('statsbot-backend')
const snekfetch = require('snekfetch')
const humanise = require('humanize-plus')
const pad = require('pad')

const { distanceTo, getSystemData, formatCoords } = require('../edsm/helpers.js')

// JSDoc Typing
const Discord = require('discord.js')
const Command = Backend.Command // eslint-disable-line
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = async data => {
  if (data.arguments.length === 0) {
    const error = new Backend.MessageBuilder()
      .setName('EDSM')
      .setError()
      .setMessage('You didn\'t specify a system name!')
      .formattedMessage
    data.message.channel.send(error)
  } else {
    data.message.channel.startTyping()
    let systemName = data.arguments.join(' ')
    try {
      // Get system data
      let systemData = await getSystemData(systemName)

      let calculated = [
        { name: 'Sol', coords: { x: 0, y: 0, z: 0 } },
        { name: 'Maia', coords: { x: -81.78125, y: -149.4375, z: -343.375 } },
        { name: 'Colonia', coords: { x: -9530.5, y: -910.28125, z: 19808.125 } },
        { name: 'Sagittarius A*', coords: { x: 25.21875, y: -20.90625, z: 25899.96875 } },
        { name: 'Beagle Point', coords: { x: -1111.5625, y: -134.21875, z: 65269.75 } },
      ].map(x => {
        x.distance = distanceTo(systemData.coords, x.coords)
        return x
      }).filter(x => x.distance !== 0)
        .sort((a, b) => a.distance - b.distance)
      let distances = calculated.map(x => {
        let str = ''
        str += pad(`Distance to ${x.name}:`, 28)
        str += `${humanise.formatNumber(x.distance, 2)}Ly`
        return str
      })

      // Grab Role Colour (if guild)
      let self = data.message.guild === null ? null : await data.message.guild.members.get(data.client.user.id)
      let colour = self !== null && self.displayColor

      const embed = new Discord.MessageEmbed()
        .setColor(colour)
        .setTitle(systemData.name)
        .setDescription(`EDSM Data for ${systemData.name}`)
        .setURL(`https://www.edsm.net/en/system/id/${systemData.id}/name/${encodeURIComponent(systemData.name)}`)
        .addField('Distances', `\`\`\`${distances.join('\n')}\`\`\``, true)
        .addField('Coordinates:', formatCoords(systemData.coords), true)
        .setTimestamp()
        .setThumbnail('https://i.imgur.com/syM3Mna.png')

      try {
        const authorRes = await snekfetch.get(`https://edsm-first-discovery-vc7qua0c5u7r.runkit.sh/api/v1.0/system/${systemData.id}/${systemData.name}`) // eslint-disable-line
        let { cmdrName, cmdrURL } = authorRes.body
        embed.setAuthor(`First discovered by ${cmdrName}`, 'https://i.imgur.com/xSAbtax.png', cmdrURL)
      } catch (err) {
        console.error(err)
        // No-op
      }

      if (systemData.primaryStar !== null) {
        embed.addField('Main Star:', `**Star Class:** *${systemData.primaryStar.type.replace(' Star', '')}*
**Scoopable:** *${systemData.primaryStar.isScoopable ? 'Yes' : 'No'}*`, true)
      }
      if (systemData.requirePermit) embed.addField('Permit Locked:', `Requires **${systemData.permitName}** Permit`, true)

      await data.message.channel.stopTyping()
      data.message.channel.send('', { embed })
    } catch (err) {
      const error = new Backend.MessageBuilder()
        .setName('EDSM')
        .setError()
        .setMessage(`System **${systemName}** not found.`)
        .formattedMessage

      await data.message.channel.stopTyping()
      data.message.channel.send(error)
    }
  }
}

const EDSM = new Backend.Command()
  .setName('edsm')
  .setDescription('Queries EDSM for System Info')
  .addArgument('systemName')
  .setMain(main)

module.exports = EDSM

const Backend = require('statsbot-backend')
const humanise = require('humanize-plus')

const { distanceTo, getSystemData, formatCoords } = require('../edsm/helpers.js')

// JSDoc Typing
const Discord = require('discord.js')
const Command = Backend.Command // eslint-disable-line
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = async data => {
  if (data.arguments.length < 2) {
    const error = new Backend.MessageBuilder()
      .setName('Distance')
      .setError()
      .setMessage('You didn\'t specify enough system names!')
      .formattedMessage
    data.message.channel.send(error)
  } else {
    data.message.channel.startTyping()
    let [systemA, systemB] = data.arguments
    try {
      // Calculate distance
      let [dataA, dataB] = await Promise.all([getSystemData(systemA), getSystemData(systemB)])
      let distance = distanceTo(dataA.coords, dataB.coords)

      // Grab Role Colour (if guild)
      let self = data.message.guild === null ? null : await data.message.guild.members.get(data.client.user.id)
      let colour = self !== null && self.displayColor

      // Generate Embed
      const embed = new Discord.MessageEmbed()
        .setColor(colour)
        .setTitle('EDSM Distance Calculator')
        .setDescription(`Distance between ${dataA.name} and ${dataB.name}`)
        .addField('Distance', `${humanise.formatNumber(distance, 2)}Ly`)
        .addField(`${dataA.name} Coordinates:`, formatCoords(dataA.coords), true)
        .addField(`${dataB.name} Coordinates:`, formatCoords(dataB.coords), true)
        .setTimestamp()
        .setThumbnail('https://i.imgur.com/syM3Mna.png')

      await data.message.channel.stopTyping()
      data.message.channel.send('', { embed })
    } catch (err) {
      const error = new Backend.MessageBuilder()
        .setName('EDSM')
        .setError()
        .setMessage(`One of the systems was not found.`)
        .formattedMessage

      await data.message.channel.stopTyping()
      data.message.channel.send(error)
    }
  }
}

const Distance = new Backend.Command()
  .setName('distance')
  .setDescription('Calculates distanced between two Systems')
  .addArgument('systemA')
  .addArgument('systemB')
  .setMain(main)

module.exports = Distance

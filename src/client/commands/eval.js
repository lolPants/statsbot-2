const Backend = require('statsbot-backend')

// JSDoc Typing
const Discord = require('discord.js')
const Command = Backend.Command // eslint-disable-line
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = data => {
  let client = data.client // eslint-disable-line

  let full = data.arguments.join(' ')

  let result, emoji, output, finalMsg
  try {
    result = eval(`(function() { return ${full}; }())`)
    emoji = ':white_check_mark:'
  } catch (err) {
    result = `${err.name}: ${err.message}.`
    emoji = ':x:'
  }

  // Try to parse JSON Object
  try {
    output = JSON.stringify(result, null, 2)
  } catch (error) {
    output = result
  }

  // Make up nice message
  let newMsg = `**Evaluating:**\n:arrow_right: \`Input\`\n\`\`\`js
${full}\`\`\`\n**Returns: **\n${emoji} \`Output\`\n\`\`\`\`js\n${output}\n\`\`\``

  // Check the message fits.
  if (newMsg.length > 1980) {
    finalMsg = `**Evaluating:**\n:arrow_right: \`Input\`
\`\`\`js\n${full}\`\`\`\n**Returns: **\n${emoji} \`Output\`\n\`\`\`\`js\nERROR: Too Long\n\`\`\``
  } else {
    finalMsg = newMsg
  }

  data.message.channel.send(finalMsg)
}

const Eval = new Backend.Command()
  .setName('eval')
  .setOwner()
  .addArgument('js')
  .setDescription('Evaluate JavaScript Code')
  .setMain(main)

module.exports = Eval

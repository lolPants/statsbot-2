const Backend = require('statsbot-backend')

// JSDoc Typing
const Discord = require('discord.js')
const Command = Backend.Command // eslint-disable-line
const BackendClient = Backend.Client // eslint-disable-line
const DiscordMessage = Discord.Message // eslint-disable-line

/**
 * @typedef {Object} CommandProperties
 * @property {BackendClient} client
 * @property {DiscordMessage} message
 * @property {Command} command
 * @property {string[]} arguments
 */

/**
 * @param {CommandProperties} data Command Properties
 */
const main = data => { data.message.channel.send('<:AnimuDabLeft:320140270710816769> <:AnimuDabRight:321985852987408384>') }

const Dab = new Backend.Command()
  .setName('dab')
  .setOwner()
  .setDescription('ULTIMATE DABERONI')
  .setMain(main)

module.exports = Dab

const Backend = require('statsbot-backend')

const Dab = require('./commands/dab')
const Eval = require('./commands/eval')
const Invite = require('./commands/invite')
const { FetchPC, FetchXBL, FetchPSN } = require('./commands/fetch')
const EDSM = require('./commands/edsm')
const Distance = require('./commands/distance')

const Registry = new Backend.Registry()
  .addCommand(Dab)
  .addCommand(Eval)
  .addCommand(Invite)
  .addCommand(FetchPC)
  .addCommand(FetchXBL)
  .addCommand(FetchPSN)
  .addCommand(EDSM)
  .addCommand(Distance)

module.exports = Registry
